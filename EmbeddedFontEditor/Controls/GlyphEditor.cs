﻿using EmbeddedFontEditor.Controls.Classes;
using EmbeddedFontEditor.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace EmbeddedFontEditor.Controls
{
    public class GlyphEditor : FrameworkElement
    {
        // Private constants --------------------------------------------------

        private const float WORKING_AREA_SIZE_MULTIPLIER = 2.0f;

        // Private classes ----------------------------------------------------

        private enum State
        {
            Idle,
            Pan,
            Draw
        }

        private class StateData
        {

        }

        private class PanStateData : StateData
        {
            public PanStateData(AbsolutePoint heldPoint)
            {
                HeldPoint = heldPoint;
            }

            public AbsolutePoint HeldPoint { get; }
        }

        private class DrawStateData : StateData
        {
            public DrawStateData(byte brush)
            {
                Brush = brush;
            }

            public byte Brush { get; }
        }

        private class Metrics
        {
            public class ConstraintsData
            {
                public ConstraintsData(AbsoluteRect topLeftConstraints)
                {
                    this.TopLeftConstraints = topLeftConstraints;
                }

                public AbsoluteRect TopLeftConstraints { get; }
            }

            public class AreasData
            {
                public AreasData(AbsoluteRect workingArea)
                {
                    this.WorkingArea = workingArea;
                }

                public AbsoluteRect WorkingArea { get; }
            }

            public class ViewportData
            {
                public ViewportData(AbsoluteSize viewportSize)
                {
                    this.ViewportSize = viewportSize;
                }

                public AbsoluteSize ViewportSize { get; }
            }

            // Private fields

            private readonly FrameworkElement host;

            private ConstraintsData constraints = null;
            private bool constraintsValid = false;
            private AreasData areas = null;
            private bool areasValid = false;
            private ViewportData viewport = null;
            private bool viewportValid = false;
            private GlyphModel glyph = null;

            private float scale = 1.0f;
            private AbsolutePoint topLeft = new AbsolutePoint(0.0f, 0.0f);
            
            // Private methods

            private void ClampScale()
            {
                scale = Math.Max(2.0f, scale);
            }

            private void ClampTopLeft()
            {
                topLeft = new AbsolutePoint(Math.Max(constraints.TopLeftConstraints.Left, Math.Min(constraints.TopLeftConstraints.Right, topLeft.X)),
                    Math.Max(constraints.TopLeftConstraints.Top, Math.Min(constraints.TopLeftConstraints.Bottom, topLeft.Y)));
            }

            private void ValidateConstraints()
            {
                if (constraintsValid)
                    return;

                ValidateAreas();
                ValidateViewport();

                if (glyph == null)
                {
                    var topLeftConstraints = new AbsoluteRect(0.0f, 0.0f, 0.0f, 0.0f);

                    constraints = new ConstraintsData(topLeftConstraints);

                    ClampTopLeft();
                }
                else
                {
                    AbsolutePoint topLeftCenter = new AbsolutePoint(areas.WorkingArea.Left + areas.WorkingArea.Width / 2.0f - viewport.ViewportSize.Width / 2.0f,
                        areas.WorkingArea.Top + areas.WorkingArea.Height / 2.0f - viewport.ViewportSize.Height / 2.0f);
                    AbsoluteSize topLeftSize = new AbsoluteSize(Math.Max(0.0f, areas.WorkingArea.Width - viewport.ViewportSize.Width),
                        Math.Max(0.0f, areas.WorkingArea.Height - viewport.ViewportSize.Height));

                    var topLeftConstraints = new AbsoluteRect(topLeftCenter.X - topLeftSize.Width / 2.0f, 
                        topLeftCenter.Y - topLeftSize.Height / 2.0f,
                        topLeftCenter.X + topLeftSize.Width / 2.0f,
                        topLeftCenter.Y + topLeftSize.Height / 2.0f);

                    constraints = new ConstraintsData(topLeftConstraints);

                    ClampTopLeft();
                }

                constraintsValid = true;
            }

            private void ValidateViewport()
            {
                if (viewportValid)
                    return;

                var viewportSize = new AbsoluteSize((float)(host.RenderSize.Width / scale), (float)(host.RenderSize.Height / scale));
                viewport = new ViewportData(viewportSize);

                viewportValid = true;
            }

            private void ValidateAreas()
            {
                if (areasValid)
                    return;

                if (glyph == null)
                {
                    areas = new AreasData(new AbsoluteRect(0, 0, 0, 0));
                }
                else
                {
                    float workingAreaMargin = (Math.Max(glyph.Width, glyph.Height) * WORKING_AREA_SIZE_MULTIPLIER - Math.Max(glyph.Width, glyph.Height)) / 2;
                    float workingAreaWidth = glyph.Width + 2 * workingAreaMargin;
                    float workingAreaHeight = glyph.Height + 2 * workingAreaMargin;
                    var workingAreaRect = new AbsoluteRect(-workingAreaMargin, -workingAreaMargin, glyph.Width + workingAreaMargin, glyph.Height + workingAreaMargin);

                    areas = new AreasData(workingAreaRect);
                }                

                areasValid = true;
            }

            private void SetScale(float newScale)
            {
                scale = newScale;
                ClampScale();

                InvalidateViewport();
                InvalidateConstraints();
            }

            // Public methods

            public Metrics(FrameworkElement host)
            {
                this.host = host ?? throw new ArgumentNullException(nameof(host));
            }

            public void Invalidate()
            {
                InvalidateConstraints();
                InvalidateAreas();
                InvalidateViewport();
            }

            public void InvalidateConstraints()
            {
                constraintsValid = false;
            }

            public void InvalidateAreas()
            {
                areasValid = false;

                InvalidateViewport();
            }

            public void InvalidateViewport()
            {
                viewportValid = false;
            }

            public void Validate()
            {
                if (!areasValid)
                    ValidateAreas();
                if (!viewportValid)
                    ValidateViewport();
                if (!constraintsValid)
                    ValidateConstraints();
            }

            internal void Center()
            {
                topLeft = constraints.TopLeftConstraints.Center;
            }

            // Public properties

            public float Scale
            {
                get
                {
                    return scale;
                }
                set
                {
                    SetScale(value);
                }
            }

            public AbsolutePoint TopLeft
            {
                get
                {
                    return topLeft;
                }
                set
                {
                    topLeft = value;
                    ClampTopLeft();
                }
            }

            public GlyphModel Glyph
            {
                get
                {
                    return glyph;
                }
                set
                {
                    glyph = value;
                    Invalidate();
                }
            }

            public ConstraintsData Constraints => constraints;
            public ViewportData Viewport => viewport;
            public AreasData Areas => areas;

            public bool Valid => constraintsValid && areasValid && viewportValid;
        }

        // Private fields -----------------------------------------------------

        private GlyphModel glyph = null;

        // Metrics

        private readonly Metrics metrics;

        // Absolute = Screen / scale
        // Screen = Absolute * scale
        // scale = Screen / Absolute

        private int mouseX = 0;
        private int mouseY = 0;

        // State

        private State state = State.Idle;
        private StateData stateData = null;

        // Private methods ----------------------------------------------------

        private AbsolutePoint ScreenToAbsolute(ScreenPoint point)
        {
            return new AbsolutePoint(point.X / metrics.Scale + metrics.TopLeft.X, 
                point.Y / metrics.Scale + metrics.TopLeft.Y);
        }

        private ScreenPoint AbsoluteToScreen(AbsolutePoint point)
        {
            return new ScreenPoint((point.X - metrics.TopLeft.X) * metrics.Scale, 
                (point.Y - metrics.TopLeft.Y) * metrics.Scale);
        }

        private AbsoluteRect ScreenToAbsolute(ScreenRect rect)
        {
            return new AbsoluteRect(rect.Left / metrics.Scale + metrics.TopLeft.X,
                rect.Top / metrics.Scale + metrics.TopLeft.Y,
                rect.Right / metrics.Scale + metrics.TopLeft.X,
                rect.Bottom / metrics.Scale + metrics.TopLeft.Y);
        }

        private ScreenRect AbsoluteToScreen(AbsoluteRect rect)
        {
            return new ScreenRect((rect.Left - metrics.TopLeft.X) * metrics.Scale,
                (rect.Top - metrics.TopLeft.Y) * metrics.Scale,
                (rect.Right - metrics.TopLeft.X) * metrics.Scale,
                (rect.Bottom - metrics.TopLeft.Y) * metrics.Scale);
        }

        private void PaintBackground(DrawingContext drawingContext)
        {
            Brush brush = new SolidColorBrush(Color.FromRgb(220, 220, 220));
            drawingContext.DrawRectangle(brush, null, new Rect(0, 0, RenderSize.Width, RenderSize.Height));
        }
       
        private void PaintGlyph(DrawingContext drawingContext)
        {
            ScreenRect glyphRect = AbsoluteToScreen(new AbsoluteRect(0, 0, glyph.Width, glyph.Height));
            Brush glyphBackgroundBrush = new SolidColorBrush(Color.FromRgb(255, 255, 255));
            drawingContext.DrawRectangle(glyphBackgroundBrush, null, glyphRect.ToRect());

            int minY = Math.Max(0, (int)Math.Floor(metrics.TopLeft.Y));
            int maxY = Math.Min((int)glyph.Height - 1, (int)Math.Floor(metrics.TopLeft.Y + metrics.Viewport.ViewportSize.Height));
            int minX = Math.Max(0, (int)Math.Floor(metrics.TopLeft.X));
            int maxX = Math.Min((int)glyph.Width - 1, (int)Math.Floor(metrics.TopLeft.X + metrics.Viewport.ViewportSize.Width));

            for (int y = minY; y <= maxY; y++)
                for (int x = minX; x <= maxX; x++)
                {
                    if (glyph.Data[x, y] == 1)
                    {
                        ScreenRect rect = AbsoluteToScreen(new AbsoluteRect(x, y, x + 1, y + 1));

                        Brush pixelBrush = new SolidColorBrush(Color.FromRgb(0, 0, 0));
                        drawingContext.DrawRectangle(pixelBrush, null, rect.ToRect());
                    }
                }

            if (metrics.Scale > 4.0f)
            {
                // Paint grid

                Pen gridPen = new Pen(new SolidColorBrush(Color.FromRgb(192, 192, 192)), 1.0);

                // Vertical lines

                for (int x = minX; x <= maxX + 1; x++)
                {
                    ScreenPoint from = AbsoluteToScreen(new AbsolutePoint(x, 0));
                    ScreenPoint to = AbsoluteToScreen(new AbsolutePoint(x, glyph.Height));

                    drawingContext.DrawLine(gridPen, from.ToPoint(), to.ToPoint());
                }

                // Horizontal lines

                for (int y = minY; y <= maxY + 1; y++)
                {
                    ScreenPoint from = AbsoluteToScreen(new AbsolutePoint(0, y));
                    ScreenPoint to = AbsoluteToScreen(new AbsolutePoint(glyph.Width, y));

                    drawingContext.DrawLine(gridPen, from.ToPoint(), to.ToPoint());
                }
            }
        }

        private void PaintOverlays(DrawingContext drawingContext)
        {
            // Delta

            Pen deltaPen = new Pen(new SolidColorBrush(Color.FromRgb(45, 176, 255)), 1.0f);

            ScreenPoint point1 = AbsoluteToScreen(new AbsolutePoint(glyph.Delta - glyph.OffsetX, 0));
            ScreenPoint point2 = AbsoluteToScreen(new AbsolutePoint(glyph.Delta - glyph.OffsetX, glyph.Height));

            drawingContext.DrawLine(deltaPen, point1.ToPoint(), point2.ToPoint());

            // Cursor

            Pen cursorPen = new Pen(new SolidColorBrush(Color.FromRgb(255, 72, 104)), 1.0f);

            ScreenPoint cursorPoint = new ScreenPoint(-glyph.OffsetX, -glyph.OffsetY);

            point1 = AbsoluteToScreen(new AbsolutePoint(cursorPoint.X, cursorPoint.Y - 2));
            point2 = AbsoluteToScreen(new AbsolutePoint(cursorPoint.X, cursorPoint.Y + 2));
            drawingContext.DrawLine(cursorPen, point1.ToPoint(), point2.ToPoint());

            point1 = AbsoluteToScreen(new AbsolutePoint(cursorPoint.X - 2, cursorPoint.Y));
            point2 = AbsoluteToScreen(new AbsolutePoint(cursorPoint.X + 2, cursorPoint.Y));
            drawingContext.DrawLine(cursorPen, point1.ToPoint(), point2.ToPoint());
        }

        private void PaintGlyphNotUsed(DrawingContext drawingContext)
        {
            FormattedText text = new FormattedText("Glyph not used",
                    CultureInfo.InvariantCulture,
                    FlowDirection.LeftToRight,
                    new Typeface("Calibri"),
                    24.0,
                    new SolidColorBrush(Color.FromRgb(255, 64, 64)));

            Point point = new Point(RenderSize.Width / 2.0 - text.Width / 2.0,
                RenderSize.Height / 2.0 - text.Height / 2.0);

            drawingContext.DrawText(text, point);
        }

        private void PaintMouseCoords(DrawingContext drawingContext)
        {
            FormattedText text = new FormattedText($"Mouse: {mouseX}, {mouseY}", CultureInfo.InvariantCulture, FlowDirection.LeftToRight, new Typeface("Calibri"), 12.0, Brushes.Black);

            int footerMargin = 4;

            Rect rect = new Rect(0, RenderSize.Height - (text.Height + 2 * footerMargin), text.Width + 2 * footerMargin, text.Height + 2 * footerMargin);
            drawingContext.DrawRectangle(new SolidColorBrush(Color.FromRgb(255, 255, 255)), null, rect);

            drawingContext.DrawText(text, new Point(footerMargin, RenderSize.Height - (text.Height + footerMargin)));
        }

        private void ZeroView()
        {
            metrics.Validate();

            if (glyph != null)
            {
                metrics.Center();
                InvalidateVisual();
            }
        }

        private void HandleGlyphChanged(object sender, EventArgs e)
        {
            InvalidateVisual();
        }

        private void HandleGlyphSizeChanged(object sender, EventArgs e)
        {
            metrics.Invalidate();
            InvalidateVisual();
        }

        private void FloodFill(int x, int y)
        {
            byte search, replace;

            if (x < 0 || x >= glyph.Data.GetLength(0) || y < 0 || y >= glyph.Data.GetLength(1))
                return;

            if (glyph.Data[x, y] == 1)
            {
                search = 1;
                replace = 0;
            }
            else
            {
                search = 0;
                replace = 1;
            }

            Stack<IntPoint> stack = new Stack<IntPoint>();
            stack.Push(new IntPoint(x, y));

            while (stack.Count > 0)
            {
                IntPoint point = stack.Pop();

                glyph.Data[point.X, point.Y] = replace;

                if (point.X - 1 >= 0 && glyph.Data[point.X - 1, point.Y] == search)
                    stack.Push(new IntPoint(point.X - 1, point.Y));
                if (point.X + 1 < glyph.Data.GetLength(0) && glyph.Data[point.X + 1, point.Y] == search)
                    stack.Push(new IntPoint(point.X + 1, point.Y));
                if (point.Y - 1 >= 0 && glyph.Data[point.X, point.Y - 1] == search)
                    stack.Push(new IntPoint(point.X, point.Y - 1));
                if (point.Y + 1 < glyph.Data.GetLength(1) && glyph.Data[point.X, point.Y + 1] == search)
                    stack.Push(new IntPoint(point.X, point.Y + 1));
            }

            glyph.NotifyChanged();
        }

        // Protected methods --------------------------------------------------

        protected override void OnRender(DrawingContext drawingContext)
        {
            metrics.Validate();

            drawingContext.PushClip(new RectangleGeometry(new Rect(0, 0, RenderSize.Width, RenderSize.Height)));

            PaintBackground(drawingContext);

            if (glyph != null)
            {
                if (glyph.Used)
                {
                    PaintGlyph(drawingContext);
                    PaintOverlays(drawingContext);
                }
                else
                {
                    PaintGlyphNotUsed(drawingContext);
                }

                PaintMouseCoords(drawingContext);
            }

            drawingContext.Pop();
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            if (glyph == null)
                return;

            if (state == State.Idle && e.ChangedButton == MouseButton.Left)
            {
                // Possibly start drawing
                AbsolutePoint clickPoint = ScreenToAbsolute(new ScreenPoint((float)e.GetPosition(this).X, (float)e.GetPosition(this).Y));
                int x = (int)Math.Floor(clickPoint.X);
                int y = (int)Math.Floor(clickPoint.Y);

                if (x >= 0 && y >= 0 && x < glyph.Width && y < glyph.Height)
                {
                    byte brush;
                    if (glyph.Data[x, y] == 0)
                        brush = 1;
                    else
                        brush = 0;

                    glyph.Data[x, y] = brush;

                    state = State.Draw;
                    stateData = new DrawStateData(brush);
                }

                InvalidateVisual();
            }
            else if (state == State.Idle && e.ChangedButton == MouseButton.Middle)
            {
                // Start panning
                AbsolutePoint clickPoint = ScreenToAbsolute(new ScreenPoint((float)e.GetPosition(this).X, (float)e.GetPosition(this).Y));

                state = State.Pan;
                stateData = new PanStateData(clickPoint);
            }
            else if (state == State.Idle && e.ChangedButton == MouseButton.Right)
            {
                AbsolutePoint clickPoint = ScreenToAbsolute(new ScreenPoint((float)e.GetPosition(this).X, (float)e.GetPosition(this).Y));

                FloodFill((int)clickPoint.X, (int)clickPoint.Y);
            }
        }

        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            if (state == State.Draw && e.ChangedButton == MouseButton.Left)
            {
                state = State.Idle;
                stateData = null;
            }
            else if (state == State.Pan && e.ChangedButton == MouseButton.Middle)
            {
                state = State.Idle;
                stateData = null;
            }
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            // Mouse coords
            if (glyph != null)
            {
                AbsolutePoint currentMousePoint = ScreenToAbsolute(new ScreenPoint((float)e.GetPosition(this).X, (float)e.GetPosition(this).Y));
                int newMouseX = (int)currentMousePoint.X;
                int newMouseY = (int)currentMousePoint.Y;

                if (newMouseX >= 0 && newMouseX < glyph.Width &&
                    newMouseY >= 0 && newMouseY < glyph.Height &&
                    (newMouseX != mouseX || newMouseY != mouseY)) 
                {
                    mouseX = newMouseX;
                    mouseY = newMouseY;

                    InvalidateVisual();
                }
            }

            if (state == State.Draw)
            {
                AbsolutePoint mousePoint = ScreenToAbsolute(new ScreenPoint((float)e.GetPosition(this).X, (float)e.GetPosition(this).Y));
                int x = (int)Math.Floor(mousePoint.X);
                int y = (int)Math.Floor(mousePoint.Y);

                if (x >= 0 && y >= 0 && x < glyph.Width && y < glyph.Height)
                {
                    glyph.Data[x, y] = ((DrawStateData)stateData).Brush;
                    InvalidateVisual();
                }
            }
            else if (state == State.Pan)
            {
                ScreenPoint mousePoint = new ScreenPoint((float)e.GetPosition(this).X, (float)e.GetPosition(this).Y);
                var heldPoint = ((PanStateData)stateData).HeldPoint;

                metrics.TopLeft = new AbsolutePoint(heldPoint.X - mousePoint.X / metrics.Scale, heldPoint.Y - mousePoint.Y / metrics.Scale);
                
                InvalidateVisual();
            }
        }

        protected override void OnMouseWheel(MouseWheelEventArgs e)
        {
            if (state == State.Idle)
            {
                // Store center point
                AbsolutePoint center = ScreenToAbsolute(new ScreenPoint((float)(RenderSize.Width / 2.0f), (float)(RenderSize.Height / 2.0f)));

                // Change scale
                if (e.Delta > 0)
                    metrics.Scale = (float)(metrics.Scale * Math.Pow(1.1f, e.Delta / 120.0f));
                else
                    metrics.Scale = (float)(metrics.Scale * Math.Pow(0.9f, -e.Delta / 120.0f));

                // Restore center point
                metrics.TopLeft = new AbsolutePoint(center.X - (float)(RenderSize.Width / 2.0f) / metrics.Scale,
                    center.Y - (float)(RenderSize.Height / 2.0f) / metrics.Scale);

                InvalidateVisual();
            }
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            if (e.Property.Name == nameof(Glyph))
            {
                if (glyph != null)
                {
                    glyph.SizeChanged -= HandleGlyphSizeChanged;
                    glyph.Changed -= HandleGlyphChanged;
                }

                glyph = (GlyphModel)e.NewValue;
                metrics.Glyph = glyph;

                if (glyph != null)
                {
                    glyph.SizeChanged += HandleGlyphSizeChanged;
                    glyph.Changed += HandleGlyphChanged;
                }

                ZeroView();
            }

            base.OnPropertyChanged(e);
        }

        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        {
            base.OnRenderSizeChanged(sizeInfo);

            metrics.Invalidate();
            InvalidateVisual();
        }

        // Public methods -----------------------------------------------------

        public GlyphEditor()
        {
            metrics = new Metrics(this);
        }

        // Public properties --------------------------------------------------

        public GlyphModel Glyph
        {
            get { return (GlyphModel)GetValue(GlyphProperty); }
            set { SetValue(GlyphProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Glyph.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty GlyphProperty =
            DependencyProperty.Register(nameof(Glyph), typeof(GlyphModel), typeof(GlyphEditor), new PropertyMetadata(null));
    }
}
