﻿using EmbeddedFontEditor.Diagnostics;
using EmbeddedFontEditor.Infrastructure;
using EmbeddedFontEditor.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmbeddedFontEditor.Services.ExportFormats
{
    public static class ILI9341_t3_Exporter
    {
        private class BitTools
        {
            public static byte UnsignedBitsNeededFor(uint maxValue)
            {
                byte result = 0;
                while (maxValue > 0)
                {
                    result++;
                    maxValue >>= 1;
                }

                return Math.Max((byte)1, result);
            }

            public static byte SignedBitsNeededFor(int minValue, int maxValue)
            {
                byte maxValueBits, minValueBits;
                if (maxValue >= 0)
                    maxValueBits = (byte)(UnsignedBitsNeededFor((uint)maxValue) + 1);
                else
                    maxValueBits = (byte)(UnsignedBitsNeededFor((uint)(-maxValue - 1)) + 1);

                if (minValue >= 0)
                    minValueBits = (byte)(UnsignedBitsNeededFor((uint)minValue) + 1);
                else
                    minValueBits = (byte)(UnsignedBitsNeededFor((uint)(-minValue - 1)) + 1);

                return Math.Max(minValueBits, maxValueBits);
            }
        }

        private class GlyphStats
        {
            public GlyphStats(uint maxWidth,
                uint maxHeight,
                int minXOffset,
                int maxXOffset,
                int minYOffset,
                int maxYOffset,
                uint maxDelta)
            {
                WidthBits = BitTools.UnsignedBitsNeededFor((uint)maxWidth);
                HeightBits = BitTools.UnsignedBitsNeededFor((uint)maxHeight);
                XOffsetBits = BitTools.SignedBitsNeededFor(minXOffset, maxXOffset);

                int YOffsetAverage = (maxYOffset + minYOffset) / 2;
                CapHeight = -YOffsetAverage;
                int newMinYOffset = minYOffset + CapHeight;
                int newMaxYOffset = maxYOffset + CapHeight;

                YOffsetBits = BitTools.SignedBitsNeededFor(newMinYOffset, newMaxYOffset);
                DeltaBits = BitTools.UnsignedBitsNeededFor((uint)maxDelta);
            }

            public byte WidthBits { get; }
            public byte HeightBits { get; }
            public byte XOffsetBits { get; }
            public byte YOffsetBits { get; }
            public byte DeltaBits { get; }
            public int CapHeight { get; }
        }

        private class CharRange
        {
            public CharRange(byte codeFrom, byte codeTo)
            {
                IndexFrom = codeFrom;
                IndexTo = codeTo;
            }

            public byte IndexFrom { get; }
            public byte IndexTo { get; }
        }

        private static IEnumerable<int> CombineRanges(CharRange range1, CharRange range2)
        {
            if (range1 != null)
                for (int i = range1.IndexFrom; i <= range1.IndexTo; i++)
                    yield return i;

            if (range2 != null)
                for (int i = range2.IndexFrom; i <= range2.IndexTo; i++)
                    yield return i;
        }

        private static (CharRange, CharRange) EvaluateCharRanges(IList<GlyphModel> glyphs)
        {
            // Find biggest gap between used chars

            int? gapStart = null, gapEnd = null;
            int? biggestGapStart = null, biggestGapEnd = null;
            byte? firstChar = null, lastChar = null;

            for (int i = 0; i < glyphs.Count; i++)
            {
                if (glyphs[i] != null && glyphs[i].Used)
                {
                    if (firstChar == null)
                        firstChar = (byte)i;
                    lastChar = (byte)i;

                    if (gapStart != null)
                    {
                        // Make sure, that gapEnd points before used character
                        gapEnd = i - 1;

                        if (biggestGapStart == null || biggestGapEnd == null || gapEnd - gapStart > biggestGapEnd - biggestGapStart)
                        {
                            biggestGapStart = gapStart;
                            biggestGapEnd = gapEnd;
                        }

                        gapStart = null;
                        gapEnd = null;
                    }
                }
                else
                {
                    if (gapStart == null && firstChar != null)
                    {
                        // Make sure, that gapStart points after used character was found
                        gapStart = i;
                    }
                }
            }

            if (firstChar == null || lastChar == null)
            {
                // No characters to process
                return (null, null);
            }
            else if (biggestGapStart == null)
            {
                return (new CharRange(firstChar.Value, lastChar.Value), null);
            }
            else
            {
                return (new CharRange(firstChar.Value, (byte)(biggestGapStart - 1)), new CharRange((byte)(biggestGapEnd + 1), lastChar.Value));
            }
        }

        private static GlyphStats EvaluateGlyphStats(IList<GlyphModel> glyphs)
        {
            uint maxWidth = 0;
            uint maxHeight = 0;
            int minXOffset = 0;
            int maxXOffset = 0;
            int minYOffset = 0;
            int maxYOffset = 0;
            uint maxDelta = 0;

            bool first = true;
            for (int i = 0; i < glyphs.Count; i++)
            {
                if (glyphs[i] != null && glyphs[i].Used)
                {
                    if (first)
                    {
                        maxWidth = glyphs[i].Width;
                        maxHeight = glyphs[i].Height;
                        minXOffset = glyphs[i].OffsetX;
                        maxXOffset = glyphs[i].OffsetX;
                        minYOffset = -glyphs[i].OffsetY - (int)glyphs[i].Height;
                        maxYOffset = -glyphs[i].OffsetY - (int)glyphs[i].Height;
                        maxDelta = glyphs[i].Delta;
                        first = false;
                    }
                    else
                    {
                        maxWidth = (uint)Math.Max(maxWidth, glyphs[i].Width);
                        maxHeight = (uint)Math.Max(maxHeight, glyphs[i].Height);
                        minXOffset = Math.Min(minXOffset, glyphs[i].OffsetX);
                        maxXOffset = Math.Max(maxXOffset, glyphs[i].OffsetX);
                        minYOffset = Math.Min(minYOffset, (-glyphs[i].OffsetY - (int)glyphs[i].Height));
                        maxYOffset = Math.Max(maxYOffset, (-glyphs[i].OffsetY - (int)glyphs[i].Height));
                        maxDelta = (uint)Math.Max(maxDelta, glyphs[i].Delta);
                    }
                }
            }

            return new GlyphStats(maxWidth, maxHeight, minXOffset, maxXOffset, minYOffset, maxYOffset, maxDelta);
        }

        private static bool CheckSameRow(byte[,] rows, int y1, int y2)
        {
            for (int x = 0; x < rows.GetLength(0); x++)
                if (rows[x, y1] != rows[x, y2])
                    return false;

            return true;
        }

        private static void BuildByteData(StringBuilder sb, List<byte> data)
        {
            sb.Append("  ");

            int bytesInRow = 0;            
            for (int i = 0; i < data.Count; i++)
            {
                if (bytesInRow == 16)
                {
                    sb.AppendLine("");
                    sb.Append("  ");
                    bytesInRow = 0;
                }

                sb.Append($"0x{data[i].ToString("X2")}");
                if (i < data.Count - 1)
                    sb.Append(", ");

                bytesInRow++;
            }

            if (bytesInRow < 16)
                sb.AppendLine("");
        }

        private static (StringBuilder, StringBuilder) BuildFileContents(string filename,
            IList<GlyphModel> glyphs,
            List<byte> bitmapData,
            List<byte> indexData,
            CharRange range1,
            CharRange range2,
            byte bitsIndex,
            byte bitsWidth,
            byte bitsHeight,
            byte bitsXOffset,
            byte bitsYOffset,
            byte bitsDelta,
            byte lineSpace,
            int capHeight)
        {
            if (DebugSettings.OutputDebug)
            {
                System.Diagnostics.Debug.WriteLine($"Range1: {range1?.IndexFrom ?? 0}-{range1?.IndexTo ?? 0}");
                System.Diagnostics.Debug.WriteLine($"Range2: {range2?.IndexFrom ?? 0}-{range2?.IndexTo ?? 0}");
                System.Diagnostics.Debug.WriteLine($"BitsIndex: {bitsIndex}");
                System.Diagnostics.Debug.WriteLine($"BitsWidth: {bitsWidth}");
                System.Diagnostics.Debug.WriteLine($"BitsHeight: {bitsHeight}");
                System.Diagnostics.Debug.WriteLine($"BitsXOffset: {bitsXOffset}");
                System.Diagnostics.Debug.WriteLine($"BitsYOffset: {bitsYOffset}");
                System.Diagnostics.Debug.WriteLine($"BitsDelta: {bitsDelta}");
                System.Diagnostics.Debug.WriteLine($"LineSpace: {lineSpace}");
                System.Diagnostics.Debug.WriteLine($"CapHeight: {capHeight}");
            }

            StringBuilder cFile = new StringBuilder();
            StringBuilder hFile = new StringBuilder();

            string fontName = Path.GetFileNameWithoutExtension(filename);

            // H file

            string includeGuard = "__" + Path.GetFileName(Path.ChangeExtension(filename, "h")).Replace('.', '_') + "__";
            hFile.AppendLine($"#ifndef {includeGuard}");
            hFile.AppendLine($"#define {includeGuard}");

            hFile.AppendLine("");
            hFile.AppendLine("#include <ILI9341_t3.h>");
            hFile.AppendLine("");
            hFile.AppendLine("#ifdef __cplusplus");
            hFile.AppendLine("extern \"C\" {");
            hFile.AppendLine("#endif");
            hFile.AppendLine("");
            hFile.AppendLine($"extern const ILI9341_t3_font_t {fontName};");
            hFile.AppendLine("");
            hFile.AppendLine("#ifdef __cplusplus");
            hFile.AppendLine("} // extern \"C\"");
            hFile.AppendLine("#endif");
            hFile.AppendLine("");
            hFile.AppendLine("#endif");

            // C file

            cFile.AppendLine($"#include \"{Path.ChangeExtension(Path.GetFileName(filename), "h")}\"");
            cFile.AppendLine("");
            cFile.AppendLine($"static const unsigned char {fontName}_data[] = {{");

            BuildByteData(cFile, bitmapData);

            cFile.AppendLine($"}};");
            cFile.AppendLine($"/* Size: {bitmapData.Count} bytes */");
            cFile.AppendLine("");
            cFile.AppendLine($"static const unsigned char {fontName}_index[] = {{");

            BuildByteData(cFile, indexData);

            cFile.AppendLine("};");
            cFile.AppendLine($"/* Size: {indexData.Count} bytes */");
            cFile.AppendLine("");
            cFile.AppendLine($"const ILI9341_t3_font_t {fontName} = {{");
            cFile.AppendLine($"    {fontName}_index,");
            cFile.AppendLine($"    0,");
            cFile.AppendLine($"    {fontName}_data,");
            cFile.AppendLine($"    1,");
            cFile.AppendLine($"    0,");

            Action<CharRange> appendRange = (CharRange range) =>
            {
                if (range != null)
                {
                    cFile.AppendLine($"    {glyphs[range.IndexFrom].Code},");
                    cFile.AppendLine($"    {glyphs[range.IndexTo].Code},");
                }
                else
                {
                    cFile.AppendLine($"    0,");
                    cFile.AppendLine($"    0,");
                }
            };

            appendRange(range1);
            appendRange(range2);

            cFile.AppendLine($"    {bitsIndex},");
            cFile.AppendLine($"    {bitsWidth},");
            cFile.AppendLine($"    {bitsHeight},");
            cFile.AppendLine($"    {bitsXOffset},");
            cFile.AppendLine($"    {bitsYOffset},");
            cFile.AppendLine($"    {bitsDelta},");
            cFile.AppendLine($"    {lineSpace},");
            cFile.AppendLine($"    {capHeight}");
            cFile.AppendLine("};");

            return (hFile, cFile);
        }


        public static bool Export(IList<GlyphModel> glyphs, string fileName)
        {
            StringBuilder sb = new StringBuilder();

            (CharRange range1, CharRange range2) = EvaluateCharRanges(glyphs);
            if (range1 == null && range2 == null)
                return false;

            uint[] bitIndices = new uint[glyphs.Count];
            for (int i = 0; i < bitIndices.Length; i++)
                bitIndices[i] = 0;

            // *** Bit statistics ***
            GlyphStats stats = EvaluateGlyphStats(glyphs);

            // *** Prepare bitmap data ***
            BitBuffer buffer = new BitBuffer();

            foreach (var index in CombineRanges(range1, range2))
            {
                if (DebugSettings.OutputDebug)
                    System.Diagnostics.Debug.WriteLine($"--- Char {glyphs[index].Code} ({(char)(glyphs[index].Code)}) index {buffer.CurrentBit} ---");

                // Store current index
                if (buffer.CurrentBit % 8 != 0)
                    throw new InvalidOperationException("Data buffer is not padded to byte boundary!");

                bitIndices[index] = buffer.CurrentBit / 8;

                // Reserved: 3 bits
                buffer.PushUnsignedInt(0b000, 3);
                if (DebugSettings.OutputDebug)
                    System.Diagnostics.Debug.WriteLine(" (reserved)");

                // Glyph header
                buffer.PushUnsignedInt((uint)glyphs[index].Width, stats.WidthBits);
                if (DebugSettings.OutputDebug)
                    System.Diagnostics.Debug.WriteLine(" (Width)");
                buffer.PushUnsignedInt((uint)glyphs[index].Height, stats.HeightBits);
                if (DebugSettings.OutputDebug)
                    System.Diagnostics.Debug.WriteLine(" (Height)");
                buffer.PushSignedInt(glyphs[index].OffsetX, stats.XOffsetBits);
                if (DebugSettings.OutputDebug)
                    System.Diagnostics.Debug.WriteLine(" (XOffset)");
                buffer.PushSignedInt((-glyphs[index].OffsetY - (int)glyphs[index].Height + stats.CapHeight), stats.YOffsetBits);
                if (DebugSettings.OutputDebug)
                    System.Diagnostics.Debug.WriteLine(" (YOffset)");
                buffer.PushUnsignedInt((uint)glyphs[index].Delta, stats.DeltaBits);
                if (DebugSettings.OutputDebug)
                    System.Diagnostics.Debug.WriteLine(" (Delta)");

                // Encode rows
                int row = 0;
                while (row < glyphs[index].Data.GetLength(1))
                {
                    int repeatedRow = row;
                    while (repeatedRow < glyphs[index].Data.GetLength(1) - 2 && CheckSameRow(glyphs[index].Data, row, repeatedRow + 1) && repeatedRow - row + 1 < 9)
                        repeatedRow++;

                    if (repeatedRow == row)
                    {
                        buffer.PushBit(0);
                        if (DebugSettings.OutputDebug)
                            System.Diagnostics.Debug.Write("|");

                        for (int x = 0; x < glyphs[index].Data.GetLength(0); x++)
                            buffer.PushBit(glyphs[index].Data[x, row]);
                        if (DebugSettings.OutputDebug)
                            System.Diagnostics.Debug.WriteLine(" (Regular row)");

                        row++;
                    }
                    else
                    {
                        int count = (repeatedRow - row + 1);

                        buffer.PushBit(1);
                        if (DebugSettings.OutputDebug)
                            System.Diagnostics.Debug.Write("|");

                        buffer.PushUnsignedInt((uint)(count - 2), 3);
                        if (DebugSettings.OutputDebug)
                            System.Diagnostics.Debug.Write("|");

                        for (int x = 0; x < glyphs[index].Data.GetLength(0); x++)
                            buffer.PushBit(glyphs[index].Data[x, row]);

                        if (DebugSettings.OutputDebug)
                            System.Diagnostics.Debug.WriteLine(" (Repeated row)");

                        row = repeatedRow + 1;
                    }

                }

                // Pad to byte boundary
                buffer.PadToByte();
                if (DebugSettings.OutputDebug)
                    System.Diagnostics.Debug.WriteLine(" (Pad to byte)");
            }

            List<byte> bitmapData = buffer.Pack();
            if (DebugSettings.OutputDebug)
                System.Diagnostics.Debug.WriteLine(" (Pad to byte)");

            // *** Prepare index data ***

            // Evaluate required bit count

            // Yeah, can be done more efficiently, I'm lazy
            uint maxIndex = 0;
            for (int i = 0; i < bitIndices.Length; i++)
                maxIndex = (uint)Math.Max(maxIndex, bitIndices[i]);

            byte indexBitCount = BitTools.UnsignedBitsNeededFor(maxIndex);

            foreach (var index in CombineRanges(range1, range2))
            {
                buffer.PushUnsignedInt(bitIndices[index], indexBitCount);

                if (DebugSettings.OutputDebug)
                    System.Diagnostics.Debug.WriteLine(" (index)");
            }

            List<byte> indexData = buffer.Pack();
            if (DebugSettings.OutputDebug)
                System.Diagnostics.Debug.WriteLine(" (Pad to byte)");

            // Generate file

            // TODO evaluate line height and cap size

            (StringBuilder hFile, StringBuilder cFile) = BuildFileContents(fileName,
                glyphs,
                bitmapData,
                indexData,
                range1,
                range2,
                indexBitCount,
                stats.WidthBits,
                stats.HeightBits,
                stats.XOffsetBits,
                stats.YOffsetBits,
                stats.DeltaBits,
                0,
                stats.CapHeight);  // TODO

            string hFilename = Path.ChangeExtension(fileName, "h");
            File.WriteAllText(hFilename, hFile.ToString());
            string cFilename = Path.ChangeExtension(fileName, "cpp");
            File.WriteAllText(cFilename, cFile.ToString());

            return true;
        }
    }
}
