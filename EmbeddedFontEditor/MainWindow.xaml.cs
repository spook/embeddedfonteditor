﻿using EmbeddedFontEditor.Models;
using EmbeddedFontEditor.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using EmbeddedFontEditor.Dependencies;
using Autofac;

namespace EmbeddedFontEditor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MainWindowViewModel viewModel;

        public MainWindow()
        {
            InitializeComponent();

            viewModel = (MainWindowViewModel)Container.Instance.Resolve<MainWindowViewModel>();

            DataContext = viewModel;
        }

        private void lbGlyphs_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            (sender as ListBox).ScrollIntoView((sender as ListBox).SelectedItem);
        }

        private void Window_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.N)
            {
                e.Handled = true;
                if (viewModel.NextGlyphCommand.CanExecute(null))
                    viewModel.NextGlyphCommand.Execute(null);
            }
            else if (e.Key == Key.P)
            {
                e.Handled = true;
                if (viewModel.PreviousGlyphCommand.CanExecute(null))
                    viewModel.PreviousGlyphCommand.Execute(null);
            }
            else if (e.Key == Key.Up && (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl)))
            {
                e.Handled = true;
                if (viewModel.OriginUpCommand.CanExecute(null))
                    viewModel.OriginUpCommand.Execute(null);
            }
            else if (e.Key == Key.Down && (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl)))
            {
                e.Handled = true;
                if (viewModel.OriginDownCommand.CanExecute(null))
                    viewModel.OriginDownCommand.Execute(null);
            }
            else if (e.Key == Key.Left && (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl)))
            {
                e.Handled = true;
                if (viewModel.OriginLeftCommand.CanExecute(null))
                    viewModel.OriginLeftCommand.Execute(null);
            }
            else if (e.Key == Key.Right && (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl)))
            {
                e.Handled = true;
                if (viewModel.OriginRightCommand.CanExecute(null))
                    viewModel.OriginRightCommand.Execute(null);
            }
            else if (e.Key == Key.Left && (Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift)))
            {
                e.Handled = true;
                if (viewModel.DeltaLeftCommand.CanExecute(null))
                    viewModel.DeltaLeftCommand.Execute(null);
            }
            else if (e.Key == Key.Right && (Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift)))
            {
                e.Handled = true;
                if (viewModel.DeltaRightCommand.CanExecute(null))
                    viewModel.DeltaRightCommand.Execute(null);
            }
        }
    }
}
