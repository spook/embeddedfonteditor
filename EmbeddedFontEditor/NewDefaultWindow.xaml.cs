﻿using EmbeddedFontEditor.ViewModels;
using EmbeddedFontEditor.ViewModels.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EmbeddedFontEditor
{
    /// <summary>
    /// Logika interakcji dla klasy NewDefaultWindow.xaml
    /// </summary>
    public partial class NewDefaultWindow : Window, INewDefaultWindowAccess
    {
        private NewDefaultWindowViewModel viewModel;

        public NewDefaultWindow()
        {
            InitializeComponent();

            viewModel = new NewDefaultWindowViewModel(this);
            DataContext = viewModel;
        }

        public void CloseDialog(bool ok)
        {
            DialogResult = ok;
        }

        public NewDefaultWindowViewModel ViewModel => viewModel;
    }
}
