﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmbeddedFontEditor.Dependencies
{
    public static class Container
    {
        private static IContainer instance;

        public static IContainer Instance
        {
            get
            {
                if (instance == null)
                {
                    var builder = new ContainerBuilder();
                    builder.RegisterSource<Autofac.Features.ResolveAnything.AnyConcreteTypeNotAlreadyRegisteredSource>();
                    Configuration.Configure(builder);
                    instance = builder.Build();
                }

                return instance;
            }
        }
    }
}
