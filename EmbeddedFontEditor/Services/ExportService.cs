﻿using EmbeddedFontEditor.Enums;
using EmbeddedFontEditor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EmbeddedFontEditor.Services.ExportFormats;

namespace EmbeddedFontEditor.Services
{
    internal class ExportService : IExportService
    {
        public void Export(IList<GlyphModel> glyphs, string filename, ExportFormat format)
        {
            switch (format)
            {
                case ExportFormat.ILI9341_t3:
                    {
                        ILI9341_t3_Exporter.Export(glyphs, filename);
                        break;
                    }
                case ExportFormat.PythonPil:
                    {
                        PythonPilExporter.Export(glyphs, filename);
                        break;
                    }
                case ExportFormat.PythonPilWithClasses:
                    {
                        PythonPilExporter.ExportWithClasses(glyphs, filename);
                        break;
                    }
                case ExportFormat.EpdBitFont:
                    {
                        EpdBitFontExporter.Export(glyphs, filename);
                        break;
                    }
                default:
                    throw new InvalidOperationException("Unsupported export format!");
            }
        }
    }
}
