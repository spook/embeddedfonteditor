﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmbeddedFontEditor.Controls.Classes
{
    public class AbsolutePoint : BasePoint
    {
        public AbsolutePoint(float x, float y) 
            : base(x, y)
        {

        }
    }
}
