﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmbeddedFontEditor.ViewModels.Interfaces
{
    public interface INewFromFontWindowAccess
    {
        void CloseDialog(bool ok);
    }
}
