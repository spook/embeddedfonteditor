﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmbeddedFontEditor.Controls.Classes
{
    public class AbsoluteRect : BaseRect
    {
        public AbsoluteRect(float left, float top, float right, float bottom) 
            : base(left, top, right, bottom)
        {

        }

        public AbsolutePoint Center => new AbsolutePoint((Left + Right) / 2.0f, (Top + Bottom) / 2.0f);
    }
}
