﻿using EmbeddedFontEditor.Enums;
using EmbeddedFontEditor.Models;
using System.Collections.Generic;

namespace EmbeddedFontEditor.Services
{
    public interface IExportService
    {
        void Export(IList<GlyphModel> glyphs, string filename, ExportFormat format);
    }
}