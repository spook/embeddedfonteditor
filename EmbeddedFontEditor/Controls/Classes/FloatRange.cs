﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmbeddedFontEditor.Controls.Classes
{
    public class FloatRange
    {
        public FloatRange(float left, float right)
        {
            if (left > right)
                throw new ArgumentOutOfRangeException(nameof(right));

            Left = left;
            Right = right;
        }

        public float Left { get; }
        public float Right { get; }
    }
}
