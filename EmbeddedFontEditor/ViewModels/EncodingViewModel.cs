﻿using EmbeddedFontEditor.Attributes;
using EmbeddedFontEditor.Types;
using EmbeddedFontEditor.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace EmbeddedFontEditor.ViewModels
{
    public class EncodingViewModel : BaseViewModel
    {
        public EncodingViewModel(EncodingInfo encoding)
        {
            this.Encoding = encoding;
        }

        public EncodingInfo Encoding { get; }

        public string Display => Encoding?.DisplayName ?? "(None)";
    }
}
