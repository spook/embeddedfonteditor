﻿using EmbeddedFontEditor.Diagnostics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmbeddedFontEditor.Infrastructure
{
    public class BitBuffer
    {
        private List<byte> data = new List<byte>();
        private List<byte> bits = new List<byte>();
        uint currentBit = 0;

        public BitBuffer()
        {

        }

        public void PushBit(byte bit)
        {
            if (bit > 1)
                throw new ArgumentOutOfRangeException(nameof(bit));

            bits.Add(bit);

            if (DebugSettings.OutputDebug)
                System.Diagnostics.Debug.Write(bit);
            currentBit++;

            if (bits.Count == 8)
            {
                byte newByte = 0;
                for (int i = 0; i < 8; i++)
                {
                    newByte = (byte)((newByte << 1) | bits[i]);
                }

                data.Add(newByte);
                bits.Clear();
            }
        }

        public void PushUnsignedInt(uint value, byte bits)
        {
            if (bits > sizeof(uint) * 8)
                throw new ArgumentOutOfRangeException(nameof(bits));

            for (int i = bits; i > 0; i--)
            {
                byte bit = (byte)((value & (1 << (i - 1))) != 0 ? 1 : 0);
                PushBit(bit);
            }

            if (DebugSettings.OutputDebug)
               System.Diagnostics.Debug.Write($" [{value}]");
        }

        public void PushSignedInt(int value, byte bits)
        {
            if (bits > sizeof(int) * 8)
                throw new ArgumentOutOfRangeException(nameof(bits));

            for (int i = bits; i > 0; i--)
            {
                byte bit = (byte)((value & (1 << (i - 1))) != 0 ? 1 : 0);
                PushBit(bit);
            }

            if (DebugSettings.OutputDebug)
               System.Diagnostics.Debug.Write($" [{value}]");
        }

        public void PadToByte()
        {
            while (currentBit % 8 != 0)
                PushBit(0);
        }

        public List<byte> Pack()
        {
            // Fill unused bits
            while (bits.Count > 0)
                PushBit(0);

            var result = data;

            data = new List<byte>();
            currentBit = 0;

            return result;
        }

        public uint CurrentBit => currentBit;
    }
}
