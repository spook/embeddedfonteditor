﻿using Autofac;
using EmbeddedFontEditor.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmbeddedFontEditor.Dependencies
{
    public static class Configuration
    {
        public static void Configure(ContainerBuilder builder)
        {
            builder.RegisterType<ExportService>().As<IExportService>().SingleInstance();
        }
    }
}
