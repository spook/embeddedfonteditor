﻿using EmbeddedFontEditor.Infrastructure;
using EmbeddedFontEditor.ViewModels.Base;
using EmbeddedFontEditor.ViewModels.Interfaces;
using Spooksoft.VisualStateManager.Commands;
using Spooksoft.VisualStateManager.Conditions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace EmbeddedFontEditor.ViewModels
{
    public class NewFromFontWindowViewModel : BaseViewModel
    {
        private System.Drawing.Font font;
        private EncodingViewModel selectedEncoding;
        private readonly INewFromFontWindowAccess access;

        private void DoCancel()
        {
            access.CloseDialog(false);
        }

        private void DoOk()
        {
            access.CloseDialog(true);
        }

        private void DoChooseFont()
        {
            System.Windows.Forms.FontDialog fontDialog = new System.Windows.Forms.FontDialog();
            if (fontDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                this.font = fontDialog.Font;
                OnPropertyChanged(nameof(FontDesc));                
            }
        }

        public NewFromFontWindowViewModel(INewFromFontWindowAccess access)
        {
            this.access = access;

            BaseCondition canConfirmCondition = new LambdaCondition<NewFromFontWindowViewModel>(this, vm => vm.Font != null && vm.SelectedEncoding != null, false);

            Encodings = Encoding.GetEncodings()
                .Concat(new[] { (EncodingInfo)null })
                .Select(ei => new EncodingViewModel(ei))
                .OrderBy(ei => ei.Display)
                .ToList();
            SelectedEncoding = Encodings.First(e => e.Encoding == null);

            ChooseFontCommand = new AppCommand(obj => DoChooseFont());
            OkCommand = new AppCommand(obj => DoOk(), canConfirmCondition);
            CancelCommand = new AppCommand(obj => DoCancel());
        }

        public ICommand ChooseFontCommand { get; }
        public ICommand OkCommand { get; }
        public ICommand CancelCommand { get; }

        public System.Drawing.Font Font => font;

        public string FontDesc => Font != null ? $"{Font.Name} ({Font.Size})" : "(none)";

        public List<EncodingViewModel> Encodings { get; }
        public EncodingViewModel SelectedEncoding
        {
            get => selectedEncoding;
            set => Set(ref selectedEncoding, value);
        }
    }
}
