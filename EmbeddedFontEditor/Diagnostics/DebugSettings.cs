﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmbeddedFontEditor.Diagnostics
{
    public static class DebugSettings
    {
        public static readonly bool OutputDebug = false;
    }
}
