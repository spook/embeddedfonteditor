﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmbeddedFontEditor.Enums
{
    public enum ExportFormat
    {
        ILI9341_t3,
        PythonPil,
        PythonPilWithClasses,
        EpdBitFont
    }
}
