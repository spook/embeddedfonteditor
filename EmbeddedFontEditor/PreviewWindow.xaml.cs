﻿using EmbeddedFontEditor.Models;
using EmbeddedFontEditor.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EmbeddedFontEditor
{
    /// <summary>
    /// Interaction logic for PreviewWindow.xaml
    /// </summary>
    public partial class PreviewWindow : Window
    {
        private PreviewWindowViewModel viewModel;

        public PreviewWindow(IList<GlyphModel> glyphs, string previewString)
        {
            InitializeComponent();

            viewModel = new PreviewWindowViewModel(glyphs, previewString);
            DataContext = viewModel;
        }
    }
}
