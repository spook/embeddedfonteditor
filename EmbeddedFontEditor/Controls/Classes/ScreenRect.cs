﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace EmbeddedFontEditor.Controls.Classes
{
    public class ScreenRect : BaseRect
    {
        public ScreenRect(float left, float top, float right, float bottom) 
            : base(left, top, right, bottom)
        {

        }

        public Rect ToRect()
        {
            return new Rect(Left, Top, Width, Height);
        }
    }
}
