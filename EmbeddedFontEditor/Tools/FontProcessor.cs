﻿using EmbeddedFontEditor.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Text;

namespace EmbeddedFontEditor.Tools
{
    public static class FontProcessor
    {
        [DllImport("gdi32.dll", CharSet = CharSet.Auto)]
        private static extern bool GetTextMetrics(IntPtr hdc, out TEXTMETRICW lptm);

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        private struct TEXTMETRICW
        {
            public int tmHeight;
            public int tmAscent;
            public int tmDescent;
            public int tmInternalLeading;
            public int tmExternalLeading;
            public int tmAveCharWidth;
            public int tmMaxCharWidth;
            public int tmWeight;
            public int tmOverhang;
            public int tmDigitizedAspectX;
            public int tmDigitizedAspectY;
            public ushort tmFirstChar;
            public ushort tmLastChar;
            public ushort tmDefaultChar;
            public ushort tmBreakChar;
            public byte tmItalic;
            public byte tmUnderlined;
            public byte tmStruckOut;
            public byte tmPitchAndFamily;
            public byte tmCharSet;
        }

        [DllImport("gdi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern bool DeleteObject(IntPtr hObject);

        [DllImport("gdi32.dll", CharSet = CharSet.Auto)]
        private static extern IntPtr SelectObject(IntPtr hdc, IntPtr hgdiObj);

        private static TEXTMETRICW GetFontMetrics(IDeviceContext dc, Font font)
        {
            TEXTMETRICW result = default(TEXTMETRICW);
            IntPtr hDC;
            IntPtr hFont;
            IntPtr hFontDefault;

            hDC = IntPtr.Zero;
            hFont = IntPtr.Zero;
            hFontDefault = IntPtr.Zero;

            try
            {
                hDC = dc.GetHdc();

                hFont = font.ToHfont();
                hFontDefault = SelectObject(hDC, hFont);

                GetTextMetrics(hDC, out result);
            }
            finally
            {
                if (hFontDefault != IntPtr.Zero)
                {
                    SelectObject(hDC, hFontDefault);
                }

                if (hFont != IntPtr.Zero)
                {
                    DeleteObject(hFont);
                }

                dc.ReleaseHdc();
            }

            return result;
        }

        public static List<GlyphModel> GenerateModels(Font font, EncodingInfo encodingInfo)
        {
            var result = new List<GlyphModel>();

            for (int i = 0; i < 32; i++)
            {
                result.Add(new GlyphModel
                {
                    Code = (byte)i,
                    Delta = 1,
                    Width = 1,
                    Height = 1,
                    OffsetX = 0,
                    OffsetY = 0,
                    Used = false
                });
            }

            var encoding = encodingInfo?.GetEncoding();
            var max = encoding != null ? 256 : 127;

            TEXTMETRICW metrics;
            Bitmap bitmap = new Bitmap(1, 1, PixelFormat.Format24bppRgb);
            using (Graphics g = Graphics.FromImage(bitmap))
            {
                metrics = GetFontMetrics(g, font);

                for (int i = 32; i < max; i++)
                {
                    string chr;

                    if (encoding != null)
                    {
                        byte[] data = new byte[1];
                        data[0] = (byte)i;

                        chr = encoding.GetString(data);
                    }
                    else
                    {
                        chr = ((char)i).ToString();
                    }

                    StringFormat format = new StringFormat
                    {
                        Alignment = StringAlignment.Near,
                        FormatFlags = StringFormatFlags.FitBlackBox,
                        LineAlignment = StringAlignment.Near
                    };

                    var charSize = g.MeasureString(chr, font, new PointF(0.0f, 0.0f), format);
                    Bitmap charBitmap = new Bitmap((int)(charSize.Width), (int)(charSize.Height), PixelFormat.Format24bppRgb);
                    using (Graphics charGraphics = Graphics.FromImage(charBitmap))
                    {
                        charGraphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
                        charGraphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SingleBitPerPixelGridFit;
                        charGraphics.FillRectangle(Brushes.White, new Rectangle(0, 0, (int)charSize.Width, (int)charSize.Height));
                        charGraphics.DrawString(chr, font, Brushes.Black, new Point(0, 0), format);

                        var glyph = new GlyphModel
                        {
                            Code = (byte)i,
                            Height = (uint)charSize.Height,
                            Width = (uint)charSize.Width,
                            Delta = (uint)Math.Max(1, charSize.Width - metrics.tmExternalLeading - metrics.tmInternalLeading),
                            OffsetX = -metrics.tmInternalLeading,
                            OffsetY = 0,
                            Used = true
                        };

                        for (int y = 0; y < (int)charSize.Height; y++)
                            for (int x = 0; x < (int)charSize.Width; x++)
                            {
                                if (charBitmap.GetPixel(x, y).R < 128)
                                    glyph.Data[x, y] = 1;
                                else
                                    glyph.Data[x, y] = 0;
                            }

                        result.Add(glyph);
                    }
                }
            }

            for (int i = max + 1; i < 256; i++)
            {
                result.Add(new GlyphModel
                {
                    Code = (byte)i,
                    Delta = 1,
                    Width = 1,
                    Height = 1,
                    OffsetX = 0,
                    OffsetY = 0,
                    Used = false
                });
            }

            return result;
        }
    }
}