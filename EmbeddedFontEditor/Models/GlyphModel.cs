﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmbeddedFontEditor.Models
{
    public class GlyphModel : INotifyPropertyChanged
    {
        private byte code;
        private uint width;
        private uint height;
        private int offsetX;
        private int offsetY;
        private uint delta;
        private byte[,] data;
        private bool used;

        private void Resize(uint newWidth, uint newHeight)
        {
            var newData = new byte[newWidth, newHeight];

            for (int x = 0; x < newData.GetLength(0); x++)
                for (int y = 0; y < newData.GetLength(1); y++)
                    newData[x, y] = 0;

            for (int x = 0; x < Math.Min(width, newWidth); x++)
                for (int y = 0; y < Math.Min(height, newHeight); y++)
                    newData[x, y] = data[x, y];

            data = newData;
        }

        private void SetWidth(uint value)
        {
            if (value < 1)
                throw new ArgumentOutOfRangeException(nameof(value));

            Resize(value, height);
            width = value;

            OnSizeChanged();
        }

        private void SetHeight(uint value)
        {
            if (value < 1)
                throw new ArgumentOutOfRangeException(nameof(value));

            Resize(width, value);
            height = value;

            OnSizeChanged();
        }

        private void Reset()
        {
            width = 5;
            height = 5;
            offsetX = 0;
            offsetY = 0;
            delta = 6;
            data = new byte[5, 5];
            used = false;
            code = 0;
        }

        protected void OnChanged()
        {
            Changed?.Invoke(this, EventArgs.Empty);

        }

        protected void OnSizeChanged()
        {
            SizeChanged?.Invoke(this, EventArgs.Empty);
        }

        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public GlyphModel()
        {
            Reset();
        }

        public void NotifyChanged()
        {
            OnChanged();
        }

        public void WriteToStream(BinaryWriter writer)
        {
            try
            {
                writer.Write(width);
                writer.Write(height);
                writer.Write(offsetX);
                writer.Write(offsetY);
                writer.Write(delta);
                writer.Write(code);
                writer.Write(used);

                for (int y = 0; y < data.GetLength(1); y++)
                    for (int x = 0; x < data.GetLength(0); x++)
                        writer.Write(data[x, y]);
            }
            catch (Exception e)
            {
                throw new Exception("Cannot save glyph to stream!", e);
            }
        }

        internal void ReadFromStream(BinaryReader reader)
        {
            try
            {
                width = reader.ReadUInt32();
                if (width < 1)
                    throw new Exception("Invalid width!");
                height = reader.ReadUInt32();
                if (height < 1)
                    throw new Exception("Invalid height!");
                offsetX = reader.ReadInt32();
                offsetY = reader.ReadInt32();
                delta = reader.ReadUInt32();
                if (delta < 0)
                    throw new Exception("Invalid delta!");
                code = reader.ReadByte();
                used = reader.ReadBoolean();

                data = new byte[width, height];
                for (int y = 0; y < height; y++)
                    for (int x = 0; x < width; x++)
                    {
                        byte pixel = reader.ReadByte();
                        if (pixel != 0 && pixel != 1)
                            throw new Exception($"Invalid pixel at ({x},{y})!");

                        data[x, y] = pixel;
                    }
            }
            catch (Exception e)
            {
                Reset();
                throw new Exception("Cannot load glyph from stream!", e);
            }
        }

        public byte Code
        {
            get => code;
            set 
            {
                code = value;
                OnPropertyChanged(nameof(Code));
            }
        }

        public uint Width
        {
            get => width;
            set
            {
                SetWidth(value);
                OnPropertyChanged(nameof(Width));
            }
        }

        public uint Height
        {
            get => height;
            set
            {
                SetHeight(value);
                OnPropertyChanged(nameof(Height));
            }
        }

        public int OffsetX
        {
            get => offsetX;
            set
            {
                offsetX = value;
                OnPropertyChanged(nameof(OffsetX));
                OnChanged();
            }
        }

        public int OffsetY
        {
            get => offsetY;
            set
            {
                offsetY = value;
                OnPropertyChanged(nameof(OffsetY));
                OnChanged();
            }
        }

        public uint Delta
        {
            get => delta;
            set
            {
                delta = value;
                OnPropertyChanged(nameof(Delta));
                OnChanged();
            }
        }

        public byte[,] Data
        {
            get => data;            
        }

        public bool Used
        {
            get => used;
            set
            {
                used = value;
                OnPropertyChanged(nameof(Used));
                OnChanged();
            }
        }

        public event EventHandler SizeChanged;
        public event EventHandler Changed;
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
