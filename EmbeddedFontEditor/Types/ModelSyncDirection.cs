﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmbeddedFontEditor.Types
{
    public enum ModelSyncDirection
    {
        FromModel,
        ToModel,
        BothWays
    }
}
