﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace EmbeddedFontEditor.Controls.Classes
{
    public class ScreenPoint : BasePoint
    {
        public ScreenPoint(float x, float y) 
            : base(x, y)
        {

        }

        public Point ToPoint() => new Point(X, Y);
    }
}
