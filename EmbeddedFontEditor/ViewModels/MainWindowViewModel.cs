﻿using EmbeddedFontEditor.Enums;
using EmbeddedFontEditor.Infrastructure;
using EmbeddedFontEditor.Models;
using EmbeddedFontEditor.Services;
using EmbeddedFontEditor.Tools;
using EmbeddedFontEditor.ViewModels.Base;
using Microsoft.Win32;
using Spooksoft.VisualStateManager.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace EmbeddedFontEditor.ViewModels
{
    public class MainWindowViewModel : BaseViewModel
    {
        // Private fields -----------------------------------------------------

        private ObservableCollection<GlyphModel> glyphs;
        private string filename = null;
        private GlyphModel currentGlyph;
        private readonly IExportService exportService;

        // Private methods ----------------------------------------------------

        private void InitGlyphs(uint width = 5, uint height = 5, int offsetX = 0, int offsetY = 0, uint delta = 6)
        {
            glyphs = new ObservableCollection<GlyphModel>();

            for (int i = 0; i < 256; i++)
            {
                var glyph = new GlyphModel
                {
                    Code = (byte)i,
                    Width = width,
                    Height = height,
                    OffsetX = offsetX,
                    OffsetY = offsetY,
                    Delta = delta
                };

                glyphs.Add(glyph);
            }

            OnPropertyChanged(nameof(Glyphs));
        }

        private void OpenFont(string filename)
        {
            try
            {
                FileStream stream = new FileStream(filename, FileMode.Open, FileAccess.Read);
                BinaryReader reader = new BinaryReader(stream);

                int count = reader.ReadInt32();

                glyphs.Clear();

                for (int i = 0; i < count; i++)
                {
                    GlyphModel glyph = new GlyphModel();
                    glyph.ReadFromStream(reader);
                    glyphs.Add(glyph);
                }

                this.filename = filename;
            }
            catch
            {
                InitGlyphs();
                filename = null;

                MessageBox.Show("Cannot open embedded font file!", "Embedded font editor", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void SaveFont(string filename)
        {
            try
            {
                using (FileStream stream = new FileStream(filename, FileMode.Create))
                {
                    BinaryWriter writer = new BinaryWriter(stream);

                    int count = glyphs.Count;
                    writer.Write(count);

                    for (int i = 0; i < count; i++)
                    {
                        glyphs[i].WriteToStream(writer);
                    }

                    stream.Flush();
                }

                this.filename = filename;
            }
            catch
            {
                MessageBox.Show("Cannot save embedded font to file!", "Embedded font editor", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DoNewDefault()
        {
            NewDefaultWindow newDefaultWindow = new NewDefaultWindow();
            if (newDefaultWindow.ShowDialog() == true)
            {
                var vm = newDefaultWindow.ViewModel;

                InitGlyphs(vm.Width, vm.Height, vm.OffsetX, vm.OffsetY, vm.Delta);
                filename = null;
            }
        }

        private void DoNewFromFont()
        {
            NewFromFontWindow newFromFontWindow = new NewFromFontWindow();

            if (newFromFontWindow.ShowDialog() == true)
            {               
                var newGlyphs = FontProcessor.GenerateModels(newFromFontWindow.ViewModel.Font, newFromFontWindow.ViewModel.SelectedEncoding.Encoding);

                glyphs.Clear();
                for (int i = 0; i < newGlyphs.Count; i++)
                {
                    glyphs.Add(newGlyphs[i]);
                }

                filename = null;                              
            }
        }

        private void DoOpen()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Embedded Font Files (*.eff)|*.eff";

            if (openFileDialog.ShowDialog() == true)
            {
                OpenFont(openFileDialog.FileName);
            }
        }

        private void DoSave()
        {
            if (filename != null)
                SaveFont(filename);
            else
                DoSaveAs();
        }

        private void DoSaveAs()
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Embedded Font Files (*.eff)|*.eff";

            if (saveFileDialog.ShowDialog() == true)
            {
                SaveFont(saveFileDialog.FileName);                
            }
        }

        private void DoExportIli9341()
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Filename for .h and .cpp|*.*";

            if (saveFileDialog.ShowDialog() == true)
            {
                exportService.Export(glyphs, saveFileDialog.FileName, ExportFormat.ILI9341_t3);
            }
        }

        private void DoExportPythonPil()
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Python script (*.py)|*.py";

            if (saveFileDialog.ShowDialog() == true)
            {
                exportService.Export(glyphs, saveFileDialog.FileName, ExportFormat.PythonPil);
            }
        }

        private void DoExportPythonPilWithClasses()
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Python script (*.py)|*.py";

            if (saveFileDialog.ShowDialog() == true)
            {
                exportService.Export(glyphs, saveFileDialog.FileName, ExportFormat.PythonPilWithClasses);
            }
        }

        private void DoExportEpdBitFont()
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Filename for .h and .cpp|*.*";

            if (saveFileDialog.ShowDialog() == true)
            {
                exportService.Export(glyphs, saveFileDialog.FileName, ExportFormat.EpdBitFont);
            }
        }

        private bool ColumnEmpty(GlyphModel glyph, int column)
        {
            if (column < 0 || column >= glyph.Width)
                throw new ArgumentOutOfRangeException(nameof(column));

            for (int y = 0; y < glyph.Height; y++)
            {
                if (glyph.Data[column, y] == 1)
                    return false;
            }

            return true;
        }

        private bool RowEmpty(GlyphModel glyph, int row)
        {
            if (row < 0 || row >= glyph.Height)
                throw new ArgumentOutOfRangeException(nameof(row));

            for (int x = 0; x < glyph.Width; x++)
            {
                if (glyph.Data[x, row] == 1)
                    return false;
            }

            return true;
        }

        private void OptimizeGlyph(GlyphModel glyph)
        {
            int firstNonEmptyColumn = 0;
            while (firstNonEmptyColumn < glyph.Width - 1 && ColumnEmpty(glyph, firstNonEmptyColumn))
                firstNonEmptyColumn++;

            int lastNonEmptyColumn = (int)glyph.Width - 1;
            while (lastNonEmptyColumn > 0 && ColumnEmpty(glyph, lastNonEmptyColumn))
                lastNonEmptyColumn--;

            int firstNonEmptyRow = 0;
            while (firstNonEmptyRow < glyph.Height - 1 && RowEmpty(glyph, firstNonEmptyRow))
                firstNonEmptyRow++;

            int lastNonEmptyRow = (int)glyph.Height - 1;
            while (lastNonEmptyRow > 0 && RowEmpty(glyph, lastNonEmptyRow))
                lastNonEmptyRow--;

            if (lastNonEmptyRow < firstNonEmptyColumn)
            {
                // Empty glyph
                int newOffsetX = glyph.OffsetX + firstNonEmptyColumn;
                int newOffsetY = glyph.OffsetY + firstNonEmptyRow;

                glyph.Width = 1;
                glyph.Height = 1;
                glyph.OffsetX = newOffsetX; 
                glyph.OffsetY = 0; // This is most likely space, so offsetY doesn't matter

                glyph.NotifyChanged();
            }
            else
            {
                for (int x = 0; x < lastNonEmptyColumn - firstNonEmptyColumn + 1; x++)
                    for (int y = 0; y < lastNonEmptyRow - firstNonEmptyRow + 1; y++)
                    {
                        glyph.Data[x, y] = glyph.Data[x + firstNonEmptyColumn, y + firstNonEmptyRow];
                    }

                glyph.Width = (uint)(lastNonEmptyColumn - firstNonEmptyColumn + 1);
                glyph.Height = (uint)(lastNonEmptyRow - firstNonEmptyRow + 1);
                glyph.OffsetX += firstNonEmptyColumn;
                glyph.OffsetY += firstNonEmptyRow;

                glyph.NotifyChanged();
            }
        }

        private void DoOptimizeThis()
        {
            OptimizeGlyph(CurrentGlyph);
        }

        private void DoOptimizeAll()
        {
            foreach (var glyph in glyphs)
                OptimizeGlyph(glyph);
        }

        private void DoPreview()
        {
            PreviewWindow previewWindow = new PreviewWindow(glyphs, PreviewString);
            previewWindow.ShowDialog();
        }

        private void DoMoveAllOrigins(int param)
        {
            for (int i = 0; i < glyphs.Count; i++)
            {
                switch (param)
                {
                    case 0:
                        {
                            glyphs[i].OffsetX += 1;
                            break;
                        }
                    case 1:
                        {
                            glyphs[i].OffsetX -= 1;
                            break;
                        }
                    case 2:
                        {
                            glyphs[i].OffsetY += 1;
                            break;
                        }
                    case 3:
                        {
                            glyphs[i].OffsetY -= 1;
                            break;
                        }
                }
            }
        }

        private void DoAlignOrigin(int variant)
        {
            if (variant == 0)
            {
                // Horizontal
                int firstNonEmptyColumn = 0;
                while (firstNonEmptyColumn < CurrentGlyph.Width && ColumnEmpty(CurrentGlyph, firstNonEmptyColumn))
                    firstNonEmptyColumn++;

                CurrentGlyph.OffsetX = -firstNonEmptyColumn;
            }
            else
            {
                // Vertical
                int firstNonEmptyRow = 0;
                while (firstNonEmptyRow < CurrentGlyph.Height && RowEmpty(CurrentGlyph, firstNonEmptyRow))
                    firstNonEmptyRow++;

                CurrentGlyph.OffsetY = -firstNonEmptyRow;
            }
        }

        private void DoMoveAllDeltas(int param)
        {
            for (int i = 0; i < glyphs.Count; i++)
            {
                switch (param)
                {
                    case 0:
                        {
                            glyphs[i].Delta -= 1;
                            break;
                        }
                    case 1:
                        {
                            glyphs[i].Delta += 1;
                            break;
                        }
                }
            }
        }

        private void DoCopyFrom()
        {
            if (CopySource != null)
            {
                CurrentGlyph.Delta = CopySource.Delta;
                CurrentGlyph.OffsetX = CopySource.OffsetX;
                CurrentGlyph.OffsetY = CopySource.OffsetY;
                CurrentGlyph.Width = CopySource.Width;
                CurrentGlyph.Height = CopySource.Height;
                
                for (int y = 0; y < CurrentGlyph.Data.GetLength(1); y++)
                    for (int x = 0; x < CurrentGlyph.Data.GetLength(0); x++)
                    {
                        CurrentGlyph.Data[x, y] = CopySource.Data[x, y];
                    }

                CurrentGlyph.NotifyChanged();
            }
        }

        private void DoShift(int param)
        {
            if (CurrentGlyph == null)
                return;

            switch (param)
            {
                case 0:
                    {
                        // Left
                        for (int y = 0; y < CurrentGlyph.Data.GetLength(1); y++)
                            for (int x = 0; x < CurrentGlyph.Data.GetLength(0) - 1; x++)
                            {
                                CurrentGlyph.Data[x, y] = CurrentGlyph.Data[x + 1, y];
                            }

                        break;
                    }
                case 1:
                    {
                        // Right
                        for (int y = 0; y < CurrentGlyph.Data.GetLength(1); y++)
                            for (int x = CurrentGlyph.Data.GetLength(0) - 1; x > 0; x--)
                            {
                                CurrentGlyph.Data[x, y] = CurrentGlyph.Data[x - 1, y];
                            }

                        break;
                    }
                case 2:
                    {
                        // Top
                        for (int y = 0; y < CurrentGlyph.Data.GetLength(1) - 1; y++)
                            for (int x = 0; x < CurrentGlyph.Data.GetLength(0); x++)
                            {
                                CurrentGlyph.Data[x, y] = CurrentGlyph.Data[x, y + 1];
                            }

                        break;
                    }
                case 3:
                    {
                        // Bottom
                        for (int y = CurrentGlyph.Data.GetLength(1) - 1; y > 0; y--)
                            for (int x = 0; x < CurrentGlyph.Data.GetLength(0) - 1; x++)
                            {
                                CurrentGlyph.Data[x, y] = CurrentGlyph.Data[x, y - 1];
                            }

                        break;
                    }
            }

            CurrentGlyph.NotifyChanged();
        }

        private void DoImport()
        {
            if (CurrentGlyph == null)
                return;

            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Bitmaps (*.bmp, *.png)|*.bmp;*.png";

            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(openFileDialog.FileName);

                    CurrentGlyph.Width = (uint)bitmap.Width;
                    CurrentGlyph.Height = (uint)bitmap.Height;

                    for (int x = 0; x < bitmap.Width; x++)
                        for (int y = 0; y < bitmap.Height; y++)
                        {
                            System.Drawing.Color color = bitmap.GetPixel(x, y);
                            int avg = (color.R + color.G + color.B) / 3;
                            CurrentGlyph.Data[x, y] = avg > 128 ? (byte)0 : (byte)1;
                        }
                }
                catch
                {
                    MessageBox.Show("Cannot import image!", "Embedded Font Editor", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                }
            }
        }

        private void DoAlignOriginAndDeltaHorizontally()
        {
            for (int i = 0; i < glyphs.Count; i++)
            {
                // Origin
                int firstNonEmptyColumn = 0;
                while (firstNonEmptyColumn < glyphs[i].Width && ColumnEmpty(glyphs[i], firstNonEmptyColumn))
                    firstNonEmptyColumn++;

                // Glyph may be empty
                if (firstNonEmptyColumn >= glyphs[i].Width)
                    continue;

                glyphs[i].OffsetX = -firstNonEmptyColumn;

                // Delta

                int lastNonEmptyColumn = (int)glyphs[i].Width - 1;
                while (lastNonEmptyColumn >= 0 && ColumnEmpty(glyphs[i], (int)lastNonEmptyColumn))
                    lastNonEmptyColumn--;

                if (lastNonEmptyColumn < 0)
                    continue;

                glyphs[i].Delta = (uint)(lastNonEmptyColumn - firstNonEmptyColumn + 1);
            }
        }

        private void DoDeltaRight()
        {
            if (CurrentGlyph != null)
                CurrentGlyph.Delta += 1;
        }

        private void DoDeltaLeft()
        {
            if (CurrentGlyph != null)
                CurrentGlyph.Delta -= 1;
        }

        private void DoOriginRight()
        {
            if (CurrentGlyph != null)
                CurrentGlyph.OffsetX -= 1;
        }

        private void DoOriginLeft()
        {
            if (CurrentGlyph != null)
                CurrentGlyph.OffsetX += 1;
        }

        private void DoOriginDown()
        {
            if (CurrentGlyph != null)
                CurrentGlyph.OffsetY -= 1;
        }

        private void DoOriginUp()
        {
            if (CurrentGlyph != null)
                CurrentGlyph.OffsetY += 1;
        }

        private void DoPreviousGlyph()
        {
            int currentGlyphIndex = glyphs.IndexOf(CurrentGlyph);
            if (currentGlyphIndex > 0 && currentGlyphIndex < glyphs.Count)
                CurrentGlyph = glyphs[currentGlyphIndex - 1];
        }

        private void DoNextGlyph()
        {
            int currentGlyphIndex = glyphs.IndexOf(CurrentGlyph);
            if (currentGlyphIndex >= 0 && currentGlyphIndex < glyphs.Count - 1)
                CurrentGlyph = glyphs[currentGlyphIndex + 1];
        }


        // Protected methods --------------------------------------------------

        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        // Public methods -----------------------------------------------------

        public MainWindowViewModel(IExportService exportService)
        {
            InitGlyphs();

            NewDefaultCommand = new AppCommand(obj => DoNewDefault());
            NewFromFontCommand = new AppCommand(obj => DoNewFromFont());
            OpenCommand = new AppCommand(obj => DoOpen());
            SaveCommand = new AppCommand(obj => DoSave());
            SaveAsCommand = new AppCommand(obj => DoSaveAs());
            ExportIli9341Command = new AppCommand(obj => DoExportIli9341());
            ExportPythonPilCommand = new AppCommand(obj => DoExportPythonPil());
            ExportPythonPilWithClassesCommand = new AppCommand(obj => DoExportPythonPilWithClasses());
            ExportEpdBitFontCommand = new AppCommand(obj => DoExportEpdBitFont());
            OptimizeThisCommand = new AppCommand(obj => DoOptimizeThis());
            OptimizeAllCommand = new AppCommand(obj => DoOptimizeAll());
            PreviewCommand = new AppCommand(obj => DoPreview());
            MoveAllOriginsCommand = new AppCommand(obj => DoMoveAllOrigins(int.Parse((string)obj)));
            AlignOriginCommand = new AppCommand(obj => DoAlignOrigin(int.Parse((string)obj)));
            MoveAllDeltasCommand = new AppCommand(obj => DoMoveAllDeltas(int.Parse((string)obj)));
            ShiftCommand = new AppCommand(obj => DoShift(int.Parse((string)obj)));
            CopyFromCommand = new AppCommand(obj => DoCopyFrom());
            ImportCommand = new AppCommand(obj => DoImport());
            AlignOriginAndDeltaHorizontallyCommand = new AppCommand(obj => DoAlignOriginAndDeltaHorizontally());

            NextGlyphCommand = new AppCommand(obj => DoNextGlyph());
            PreviousGlyphCommand = new AppCommand(obj => DoPreviousGlyph());
            OriginUpCommand = new AppCommand(obj => DoOriginUp());
            OriginDownCommand = new AppCommand(obj => DoOriginDown());
            OriginLeftCommand = new AppCommand(obj => DoOriginLeft());
            OriginRightCommand = new AppCommand(obj => DoOriginRight());
            DeltaLeftCommand = new AppCommand(obj => DoDeltaLeft());
            DeltaRightCommand = new AppCommand(obj => DoDeltaRight());

            this.exportService = exportService;
        }

        // Public properties --------------------------------------------------

        public ObservableCollection<GlyphModel> Glyphs => glyphs;
        public GlyphModel CurrentGlyph
        {
            get => currentGlyph;
            set => Set(ref currentGlyph, value);
        }

        public GlyphModel CopySource { get; set; }
        public string PreviewString { get; set; }

        public ICommand NewDefaultCommand { get; }
        public ICommand NewFromFontCommand { get; }
        public ICommand OpenCommand { get; }
        public ICommand SaveCommand { get; }
        public ICommand SaveAsCommand { get; }
        public ICommand ExportIli9341Command { get; }
        public ICommand ExportPythonPilCommand { get; }
        public ICommand ExportPythonPilWithClassesCommand { get; }
        public ICommand ExportEpdBitFontCommand { get; }
        public ICommand OptimizeThisCommand { get; }
        public ICommand OptimizeAllCommand { get; }
        public ICommand PreviewCommand { get; }
        public ICommand MoveAllOriginsCommand { get; }
        public ICommand AlignOriginCommand { get; }
        public ICommand MoveAllDeltasCommand { get; }
        public ICommand CopyFromCommand { get; }
        public ICommand ShiftCommand { get; }
        public ICommand ImportCommand { get; }

        public ICommand NextGlyphCommand { get; }
        public ICommand PreviousGlyphCommand { get; }
        public ICommand OriginUpCommand { get; }
        public ICommand OriginDownCommand { get; }
        public ICommand OriginLeftCommand { get; }
        public ICommand OriginRightCommand { get; }
        public ICommand DeltaLeftCommand { get; }
        public ICommand DeltaRightCommand { get; }


        public ICommand AlignOriginAndDeltaHorizontallyCommand { get; }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
