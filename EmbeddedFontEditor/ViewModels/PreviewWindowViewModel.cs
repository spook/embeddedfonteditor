﻿using EmbeddedFontEditor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace EmbeddedFontEditor.ViewModels
{
    public class PreviewWindowViewModel
    {
        public PreviewWindowViewModel(IList<GlyphModel> glyphs, string previewString)
        {
            List<GlyphModel> previewGlyphs;
            if (String.IsNullOrEmpty(previewString))
                previewGlyphs = glyphs.ToList();
            else
                previewGlyphs = previewString
                    .Select(c => glyphs.FirstOrDefault(g => g.Code == (int)c))
                    .Where(g => g != null)
                    .ToList();

            int width = previewGlyphs
                .Where(g => g.Used)
                .Aggregate(0, (prev, glyph) => prev + (int)glyph.Delta);

            int maxWidth = (int)previewGlyphs
                .Where(g => g.Used)
                .Max(g => g.Delta);

            int maxHeight = (int)previewGlyphs
                .Where(g => g.Used)
                .Max(g => g.Height);

            var bitmap = new System.Drawing.Bitmap(width + 2 * maxWidth, 3 * maxHeight, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(bitmap))
            {
                g.FillRectangle(System.Drawing.Brushes.White, new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height));

                System.Drawing.Point cursor = new System.Drawing.Point(maxWidth, maxHeight);

                for (int i = 0; i < previewGlyphs.Count; i++)
                    if (previewGlyphs[i].Used)
                    {
                        System.Drawing.Point origin = new System.Drawing.Point(cursor.X + previewGlyphs[i].OffsetX, cursor.Y + previewGlyphs[i].OffsetY);

                        for (int x = 0; x < previewGlyphs[i].Width; x++)
                            for (int y = 0; y < previewGlyphs[i].Height; y++)
                            {
                                if (previewGlyphs[i].Data[x, y] == 1 && (origin.X + x) >= 0 && (origin.Y + y) >= 0 && (origin.X + x) < bitmap.Width && (origin.Y + y) < bitmap.Height)
                                    bitmap.SetPixel(origin.X + x, origin.Y + y, System.Drawing.Color.Black);
                            }

                        cursor = new System.Drawing.Point(cursor.X + (int)previewGlyphs[i].Delta, cursor.Y);
                    }
            }

            Image = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
               bitmap.GetHbitmap(),
               IntPtr.Zero,
               System.Windows.Int32Rect.Empty,
               BitmapSizeOptions.FromWidthAndHeight(bitmap.Width, bitmap.Height));

            Width = bitmap.Width;
            Height = bitmap.Height;
        }

        public BitmapSource Image { get; }
        public int Width { get; }
        public int Height { get; }
    }
}
