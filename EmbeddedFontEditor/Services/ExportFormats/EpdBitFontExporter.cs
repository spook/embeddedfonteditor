﻿using EmbeddedFontEditor.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmbeddedFontEditor.Services.ExportFormats
{
    public class EpdBitFontExporter
    {
        public static bool Export(IList<GlyphModel> glyphs, string fileName)
        {
            if (!glyphs.Any(g => g.Used))
                return false;

            int startGlyph = 0;
            while (!glyphs[startGlyph].Used)
                startGlyph++;

            int endGlyph = glyphs.Count - 1;
            while (!glyphs[endGlyph].Used)
                endGlyph--;

            StringBuilder hFile = new StringBuilder();
            StringBuilder cppFile = new StringBuilder();

            string fontName = Path.GetFileNameWithoutExtension(fileName);

            // H file

            string includeGuard = "__" + Path.GetFileName(Path.ChangeExtension(fileName, "h")).Replace('.', '_') + "__";
            hFile.AppendLine($"#ifndef {includeGuard}");
            hFile.AppendLine($"#define {includeGuard}");
            hFile.AppendLine("");
            hFile.AppendLine("#include \"font.h\"");
            hFile.AppendLine("");
            hFile.AppendLine("#ifdef __cplusplus");
            hFile.AppendLine("extern \"C\" {");
            hFile.AppendLine("#endif");
            hFile.AppendLine("");
            hFile.AppendLine($"extern const Display_font {fontName};");
            hFile.AppendLine("");
            hFile.AppendLine("#ifdef __cplusplus");
            hFile.AppendLine("} // extern \"C\"");
            hFile.AppendLine("#endif");
            hFile.AppendLine("");
            hFile.AppendLine("#endif");

            // CPP file

            cppFile.AppendLine($"#include \"{Path.ChangeExtension(Path.GetFileName(fileName), "h")}\"");
            cppFile.AppendLine("");

            List<int> indices = new List<int>();

            cppFile.AppendLine($"const unsigned char {fontName}Data[] = {{");

            int offset = 0;

            for (int currentGlyph = startGlyph; currentGlyph < endGlyph; currentGlyph++)
            {
                var glyph = glyphs[currentGlyph];

                indices.Add(offset);

                uint glyphByteWidth = glyph.Width / 8;
                if (glyph.Width % 8 > 0)
                    glyphByteWidth++;

                for (int y = 0; y < glyph.Height; y++)
                {
                    for (int bx = 0; bx < glyphByteWidth; bx++)
                    {
                        byte pixels = 0xFF;
                        for (int x = 0; x < 8; x++)
                        {
                            int absX = bx * 8 + x;
                            if (absX < glyph.Width && glyph.Data[absX, y] == 1)
                            {
                                pixels = (byte)(pixels & ~((byte)(0x80 >> x)));
                            }
                        }

                        cppFile.Append($"0x{pixels:X2}");
                        offset++;

                        if (currentGlyph < endGlyph - 1 || y < glyph.Height - 1 || bx < glyphByteWidth - 1)
                            cppFile.Append(", ");
                    }
                }

                cppFile.AppendLine($" // {glyph.Code}");
            }

            cppFile.AppendLine("};").AppendLine();

            cppFile.AppendLine($"const Display_font_glyph {fontName}Glyphs[] = {{");

            for (int currentGlyph = startGlyph; currentGlyph < endGlyph; currentGlyph++)
            {
                var glyph = glyphs[currentGlyph];
                cppFile.Append($"    {{");
                cppFile.Append($" {indices[currentGlyph - startGlyph]},");
                cppFile.Append($" {glyph.OffsetX},");
                cppFile.Append($" {glyph.OffsetY},");
                cppFile.Append($" {glyph.Width},");
                cppFile.Append($" {glyph.Height},");
                cppFile.Append($" {glyph.Delta}");
                cppFile.Append($" }}");
                if (currentGlyph < endGlyph - 1)
                    cppFile.AppendLine($",");
                else
                    cppFile.AppendLine();
            }
            cppFile.AppendLine($"}};").AppendLine();


            cppFile.AppendLine($"const Display_font {fontName} = {{");
            cppFile.AppendLine($"    {fontName}Data,");
            cppFile.AppendLine($"    {fontName}Glyphs,");
            cppFile.AppendLine($"    {startGlyph},");
            cppFile.AppendLine($"    {endGlyph}");
            cppFile.AppendLine($"}};");

            string hFilename = Path.ChangeExtension(fileName, "h");
            File.WriteAllText(hFilename, hFile.ToString());
            string cppFilename = Path.ChangeExtension(fileName, "cpp");
            File.WriteAllText(cppFilename, cppFile.ToString());

            return true;
        }
    }
}
