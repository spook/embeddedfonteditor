﻿using EmbeddedFontEditor.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmbeddedFontEditor.Services.ExportFormats
{
    public static class PythonPilExporter
    {
        public static void ExportWithClasses(IList<GlyphModel> glyphs, string filename)
        {
            string classPath = Path.Combine(Path.GetDirectoryName(filename), "FontPrinter.py");
            File.WriteAllText(classPath, Strings.PythonPilClasses);

            Export(glyphs, filename);
        }

        public static void Export(IList<GlyphModel> glyphs, string fileName)
        {
            StringBuilder sb = new StringBuilder();

            string fontName = Path.GetFileNameWithoutExtension(fileName);

            sb.AppendLine("from FontPrinter import FontGlyph")
                .AppendLine();

            sb.AppendLine($"class {fontName}:")
                .Append("    glyphs = {");


            bool first = true;
            foreach (var glyph in glyphs.Where(g => g.Used))
            {
                if (!first)
                    sb.AppendLine(",");
                else
                    sb.AppendLine();

                first = false;

                sb.Append($"        {glyph.Code}: FontGlyph(b'");

                for (int y = 0; y < glyph.Height; y++)
                {
                    byte b = 0;
                    int bit = 7;
                    for (int x = 0; x < glyph.Width; x++)
                    {
                        if (glyph.Data[x, y] == 0)
                            b = (byte)(b | (1 << bit));

                        bit--;

                        if (bit < 0)
                        {
                            // Emit byte
                            sb.Append($"\\x{b:X2}");
                            b = 0;
                            bit = 7;
                        }
                    }

                    if (bit != 7)
                    {
                        // We have some missing bits to emit
                        sb.Append($"\\x{b:X2}");
                    }
                }

                sb.Append($"', ({glyph.Width}, {glyph.Height}), ({glyph.OffsetX}, {glyph.OffsetY}), {glyph.Delta})");
            }
            sb.AppendLine().Append("    }");

            File.WriteAllText(fileName, sb.ToString());
        }
    }
}
