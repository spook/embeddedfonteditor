﻿using EmbeddedFontEditor.Infrastructure;
using EmbeddedFontEditor.ViewModels.Interfaces;
using Spooksoft.VisualStateManager.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace EmbeddedFontEditor.ViewModels
{
    public class NewDefaultWindowViewModel
    {
        private readonly INewDefaultWindowAccess access;

        private void DoCancel()
        {
            access.CloseDialog(false);
        }

        private void DoOk()
        {
            access.CloseDialog(true);
        }

        public NewDefaultWindowViewModel(INewDefaultWindowAccess access)
        {
            this.access = access ?? throw new ArgumentNullException(nameof(access));

            Width = 5;
            Height = 5;
            OffsetX = 0;
            OffsetY = 0;
            Delta = 6;

            OkCommand = new AppCommand(obj => DoOk());
            CancelCommand = new AppCommand(obj => DoCancel());
        }

        public ICommand OkCommand { get; }
        public ICommand CancelCommand { get; }

        public uint Width { get; set; }
        public uint Height { get; set; }
        public int OffsetX { get; set; }
        public int OffsetY { get; set; }
        public uint Delta { get; set; }
    }
}
