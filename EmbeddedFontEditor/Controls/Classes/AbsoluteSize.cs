﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmbeddedFontEditor.Controls.Classes
{
    public class AbsoluteSize : BaseSize
    {
        public AbsoluteSize(float width, float height) 
            : base(width, height)
        {

        }
    }
}
